import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';

import { OrganizationAddupdateComponent } from './organization-addupdate/organization-addupdate.component';
import { OrganizationService } from '../services/organization/organization.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
  position: string = 'left';
  displayedColumns = ['organization', 'actions'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // organizations: any;
  constructor(
    public dialog: MatDialog,
    public orgSvc: OrganizationService
  ) { }

  ngOnInit() {
    this.callOrganization();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  callOrganization(): void {
    this.orgSvc.get().subscribe(snap => {
      // console.log(snap.data);
      // this.organizations = snap.data;
      this.dataSource = snap.data;
    });
  }

  openAddUpdateDialog(counter: number): void {
    let dialogRef = this.dialog.open(OrganizationAddupdateComponent, {
      disableClose: true,
      width: '400px',
      data: {
        isAdd: counter
      }
    })
  }

}
