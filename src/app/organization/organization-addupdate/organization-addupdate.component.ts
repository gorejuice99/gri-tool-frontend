import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Organization } from '../../models/organization';
import { OrganizationService } from '../../services/organization/organization.service';

@Component({
  selector: 'app-organization-addupdate',
  templateUrl: './organization-addupdate.component.html',
  styleUrls: ['./organization-addupdate.component.scss']
})
export class OrganizationAddupdateComponent implements OnInit {
  modalTitle: string;
  organization = new Organization;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public orgSvc: OrganizationService
  ) { 
    
  }

  ngOnInit() {
    if(this.data.isAdd == 1) {
      this.modalTitle = "Add Organization";
    }
  }

  doSubmitOrganization(): void {
    this.orgSvc.add(this.organization).subscribe(res => {
      console.log(res);
    });
  }
}
