import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationAddupdateComponent } from './organization-addupdate.component';

describe('OrganizationAddupdateComponent', () => {
  let component: OrganizationAddupdateComponent;
  let fixture: ComponentFixture<OrganizationAddupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationAddupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationAddupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
