import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {
  links: Array<any> = [];
  constructor(
    public router: Router
  ) { 
    this.links = [{
      link: '/profile',
      name: 'Profile',
      iconName: 'face'
    }];
  }

  ngOnInit() {
    let user: any = JSON.parse(localStorage.getItem('currentUser'));
    if(user.userType == 0) {
      this.links.push(
        {
          link: '/super-users',
          name: 'Super Users',
          iconName: 'group'
        },
        {
          link: '/organizations',
          name: 'Organizations',
          iconName: 'group_work'
        }
      );
    } 
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    this.router.navigate(['login']);
  }

}
