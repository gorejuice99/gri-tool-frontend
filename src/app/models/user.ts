export class User {
    firstName: string;
    lastName: string;
    _id: string;
    username: string;
    password: string;
    confPassword: string;
    status: number;
    userType: number;
}