export class Disclosure {
    disclosureNumber: String;
    guidance: String;
    number: Number
    recommendations: Array<any>;
    additionalRequirements: Array<any>;
    reportingRequirements: Array<any>;
    series: Number;
    subNumber: Number;
    title: String;
}