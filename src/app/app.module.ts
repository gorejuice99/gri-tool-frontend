import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';
import { RequestOptions, XHRBackend} from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';


import { UserService } from './services/user/user.service';
import { ShellComponent } from './shell/shell.component';
import { MaterialityComponent } from './materiality/materiality.component';
import { AuthGuard } from './auth/auth.guard';

import { SuperUsersListComponent } from './super-users/super-users-list/super-users-list.component';
import { ProfileComponent } from './profile/profile.component';
import { OrganizationComponent } from './organization/organization.component';
import { SuperUsersAddupdateComponent } from './super-users/super-users-addupdate/super-users-addupdate.component';
import { OrganizationAddupdateComponent } from './organization/organization-addupdate/organization-addupdate.component';
import { OrganizationService } from './services/organization/organization.service';
import { AuthService } from './services/auth.service';
import { SuperUserMaterialityComponent } from './super-users/super-user-materiality/super-user-materiality.component';
import { DisclosureService } from './services/disclosure/disclosure.service';
import { SelectedMaterialityComponent } from './materiality/selected-materiality/selected-materiality.component';
import { DisclosureDashboardComponent } from './disclosure-dashboard/disclosure-dashboard.component';
import { SuperUserDashboardComponent } from './super-users/super-user-dashboard/super-user-dashboard.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShellComponent,
    MaterialityComponent,
    SuperUsersListComponent,
    ProfileComponent,
    OrganizationComponent,
    SuperUsersAddupdateComponent,
    OrganizationAddupdateComponent,
    SuperUserMaterialityComponent,
    SelectedMaterialityComponent,
    DisclosureDashboardComponent,
    SuperUserDashboardComponent,
  ],
  entryComponents: [
    SuperUsersAddupdateComponent,
    OrganizationAddupdateComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [UserService, AuthGuard, OrganizationService, AuthService, DisclosureService],
  bootstrap: [AppComponent]
})
export class AppModule { }
