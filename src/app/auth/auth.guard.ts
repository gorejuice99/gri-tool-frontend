import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';


import { MatSnackBar } from '@angular/material';
@Injectable() 
export class AuthGuard implements CanActivate {
  constructor(
   
    public router: Router,
    public snackBar: MatSnackBar
  ) { }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      if(localStorage.getItem('currentUser')) {
        return true;
      } else {
        this.snackBar.open('Oops, please Login first', '', {
          duration: 5000
        });

        this.router.navigate(['login']);
        return false;
      }
  }
}
