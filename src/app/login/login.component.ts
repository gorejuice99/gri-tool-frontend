import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { CoolLocalStorage } from 'angular2-cool-storage';
import { User } from '../models/user';

import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = new User
  constructor(
    public authSvc: AuthService,
    // public localStorage: CoolLocalStorage,
    public router: Router
  ) {
    this.user.username = '';
    this.user.password = '';   
  }

  ngOnInit() {
  }

  onSubmit(): void {
    this.authSvc.userLogin(this.user).subscribe(res => {
      if(res.error == 0) {
        localStorage.setItem('currentUser', JSON.stringify({
            username: res.data.username,
            _id: res.data._id,
            userType: res.data.userType,
            status: res.data.status,
            token: res.token
          }) 
        );
        
        switch(res.data.userType) {
          case 0:
            this.router.navigate(['/super-users']);
          break;
        }
        
      } else {
        console.log(res);
      }
    });
  }

}
