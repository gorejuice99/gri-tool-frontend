import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperUserMaterialityComponent } from './super-user-materiality.component';

describe('SuperUserMaterialityComponent', () => {
  let component: SuperUserMaterialityComponent;
  let fixture: ComponentFixture<SuperUserMaterialityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperUserMaterialityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperUserMaterialityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
