import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperUsersAddupdateComponent } from './super-users-addupdate.component';

describe('SuperUsersAddupdateComponent', () => {
  let component: SuperUsersAddupdateComponent;
  let fixture: ComponentFixture<SuperUsersAddupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperUsersAddupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperUsersAddupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
