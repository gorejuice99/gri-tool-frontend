import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

import { OrganizationService } from '../../services/organization/organization.service';
import { UserService } from '../../services/user/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-super-users-addupdate',
  templateUrl: './super-users-addupdate.component.html',
  styleUrls: ['./super-users-addupdate.component.scss']
})
export class SuperUsersAddupdateComponent implements OnInit {
  user = new User;
  orgList: Array<any> = [];
  modalTitle: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public orgSvc: OrganizationService,
    public userSvc: UserService
  ) { 
    this.user.userType = 1;
    this.user.status = 0;
  }

  ngOnInit() {
    // console.log(this.data);
    this.callOrganization();
    if(this.data.isAdd == 1) {
      this.modalTitle = 'Add Super User';
    }
  }

  callOrganization(): void {
    this.orgSvc.get().subscribe(snap => {
      this.orgList = snap.data;
    });
  }

  doSubmitSuperUser(): void {
    //do your thing
    // console.log(this.user);
    this.userSvc.add(this.user).subscribe(snap => {
      console.log(snap);
    });
  }

}
