import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

import { User } from '../../models/user';
import { SuperUsersAddupdateComponent } from '../super-users-addupdate/super-users-addupdate.component';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-super-users-list',
  templateUrl: './super-users-list.component.html',
  styleUrls: ['./super-users-list.component.scss']
})
export class SuperUsersListComponent implements OnInit {
  displayedColumns = ['username', 'fullname', 'organization', 'status', 'action'];
  dataSource = new MatTableDataSource<User>();
  position: string = 'left';
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    public userSvc: UserService,
    public router: Router
  ) { }

  ngOnInit() {
    this.getSuperUsers(1);
  }

  getSuperUsers(userType: number): void {
    this.userSvc.get(userType).subscribe(snap => {
      console.log(snap.data);
      this.dataSource = snap.data;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  openAddUpdateDialog(counter: number): void {
    let dialogRef = this.dialog.open(SuperUsersAddupdateComponent, {
      width: '500px',
      disableClose: true,
      data: {
        isAdd: counter
      }
    });
  }

  navigateSuperUserFunctions(id: string, command: number): void {
    if(command == 0) {
      this.router.navigate(['super-user/materiality/'+id]);
    } else if(command == 1) {
      this.router.navigate(['super-user/disclosure/'+id]);
    }
  }

}

