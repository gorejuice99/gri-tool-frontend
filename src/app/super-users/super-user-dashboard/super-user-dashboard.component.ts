import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '../../services/user/user.service';
import { User } from '../../models/user';
import { Organization } from '../../models/organization';


@Component({
  selector: 'app-super-user-dashboard',
  templateUrl: './super-user-dashboard.component.html',
  styleUrls: ['./super-user-dashboard.component.scss']
})
export class SuperUserDashboardComponent implements OnInit {
  userId: string;
  user = new User();
  organization = new Organization();
  
  constructor(
    public activatedRoute: ActivatedRoute,
    public userSvc: UserService,
   
  ) { 
    this.activatedRoute.params.subscribe(params => {
      this.userId = params.id;
    });
  }

  ngOnInit() {
    this.userSvc.getUser(this.userId).subscribe(snap => {
      console.log(snap);
      for(var x in snap.data) {
        this.user = snap.data[x];
        this.organization = snap.data[x].organization_docs[0];
      }

      
    });
  }

  

}
