import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DisclosureService } from '../services/disclosure/disclosure.service';


@Component({
  selector: 'app-disclosure-dashboard',
  templateUrl: './disclosure-dashboard.component.html',
  styleUrls: ['./disclosure-dashboard.component.scss']
})
export class DisclosureDashboardComponent implements OnInit {

  userId: string;
  genDisclosure: any; 
  ecoDisclosure: any;
  envDisclosure: any;
  socDisclosure: any;

  constructor(
    public disclosureSvc: DisclosureService,
    public activatedRoute: ActivatedRoute,
  ) { 
    this.activatedRoute.params.subscribe(params => {
      this.userId = params.id;
    });
  }

  ngOnInit() {
    this.getUserDisclosure();
  }

  getUserDisclosure(): void {
    this.disclosureSvc.getUserDisclosure(this.userId).subscribe(snap => {
      console.log(snap);
      snap.forEach(element => {
        this.ecoDisclosure = element.ecoDisclosure;
        this.envDisclosure = element.envDisclosure;
        this.socDisclosure = element.socDisclosure;
      });
    });
  }

}
