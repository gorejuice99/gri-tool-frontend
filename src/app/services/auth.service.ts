import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';

import { User } from '../models/user';
import { Config } from '../config';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AuthService {

  config = new Config;
    
  constructor(
      private http: HttpClient,
  ) { }

  userLogin(user: User): Observable<any> {
      let sendLoginVerificationLink = `${this.config.url}/api/validate_user`;

      return this.http.post(sendLoginVerificationLink, user, httpOptions).pipe(
          tap(snap => {
              return snap;
          })
      );
  }

}
