import { Injectable } from '@angular/core';


import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';

import { Config } from '../../config';

@Injectable()
export class DisclosureService {
  
  config = new Config;
  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': JSON.parse(localStorage.getItem('currentUser')).token
    })
  };

  constructor(
    private http: HttpClient,
  ) { }

  getDisclosureBySeriesNo(disclosureData: any): Observable<any> {
    let disclosureLink = `${this.config.url}/api/getDisclosureBySeriesNo`;

    return this.http.post(disclosureLink, disclosureData, this.httpOptions).pipe(
      tap(snap => {
        return snap;
      })
    );
  }

  getUserDisclosure(userId: string): Observable<any> {
    let userDisclosureLink = `${this.config.url}/api/getUserDisclosure?userId=${userId}`;

    return this.http.get(userDisclosureLink, this.httpOptions).pipe(
      tap(snap => {
        return snap;
      })
    );
  }

  saveDisclosureByUser(disclosureData: any): Observable<any> {
    let saveDisclosureLink = `${this.config.url}/api/saveDisclosureByUser`;

    return this.http.post(saveDisclosureLink, disclosureData, this.httpOptions).pipe(
      tap(snap => {
        return snap;
      })
    );  
  }

  

}
