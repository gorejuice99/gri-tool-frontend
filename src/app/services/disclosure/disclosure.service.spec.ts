import { TestBed, inject } from '@angular/core/testing';

import { DisclosureService } from './disclosure.service';

describe('DisclosureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DisclosureService]
    });
  });

  it('should be created', inject([DisclosureService], (service: DisclosureService) => {
    expect(service).toBeTruthy();
  }));
});
