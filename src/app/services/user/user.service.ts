import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';

import { User } from '../../models/user';
import { Config } from '../../config';



@Injectable()
export class UserService {
    config = new Config;
    httpOptions: any = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': JSON.parse(localStorage.getItem('currentUser')).token
        })
    };
    
    constructor(
        private http: HttpClient,
    ) { }

    get(userType: number): Observable<any> {
        let getUserLink = `${this.config.url}/api/getUser?usertype=${userType}`;

        return this.http.get(getUserLink, this.httpOptions).pipe(
            tap(snap => {
                return snap
            })
        );
    }

    add(user: User): Observable<any> {
        let addUserLink = `${this.config.url}/api/addUser`;

        return this.http.post(addUserLink, user, this.httpOptions).pipe(
            tap(snap => {
                return snap;
            })
        );
    }

    getUser(userId: string): Observable<any> {
        let getUserLink = `${this.config.url}/api/getUserPerId?id=${userId}`;

        return this.http.get(getUserLink, this.httpOptions).pipe(
            tap(snap => {
                return snap
            })
        );
    }
}

