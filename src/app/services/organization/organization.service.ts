import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';

import { Config } from '../../config';
import { Organization } from '../../models/organization';

// const 


@Injectable()
export class OrganizationService {
  config = new Config;
  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': JSON.parse(localStorage.getItem('currentUser')).token
    })
  };

  constructor(
    private http: HttpClient,
    // public config: Config
  ) { }

  get(): Observable<any> {
    let getOrganizationLink = `${this.config.url}/api/getOrganization`;
    
    return this.http.get(getOrganizationLink, this.httpOptions).pipe(
      tap(snap => {
        return snap
      })
    );
  }

  add(orgData: Organization): Observable<any> {
    let sendAddOrganiztionLink = `${this.config.url}/api/addOrganization`;
    
    return this.http.post(sendAddOrganiztionLink, orgData, this.httpOptions).pipe(
      tap(snap => {
        return snap;
      })
    );
  }

  
}
