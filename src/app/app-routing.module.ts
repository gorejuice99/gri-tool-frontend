import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ShellComponent } from './shell/shell.component';
import { MaterialityComponent }    from './materiality/materiality.component';
import { SuperUsersListComponent } from './super-users/super-users-list/super-users-list.component';
import { SuperUserMaterialityComponent } from './super-users/super-user-materiality/super-user-materiality.component';
import { ProfileComponent } from './profile/profile.component';
import { OrganizationComponent } from './organization/organization.component';
import { SuperUserDashboardComponent } from './super-users/super-user-dashboard/super-user-dashboard.component';

import { AuthGuard } from './auth/auth.guard';
const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'materiality', redirectTo: '/materiality', pathMatch: 'full' },
    {
        path: '',
        component: ShellComponent,
        canActivate: [ AuthGuard ],
        children: [
            {
                path: 'materiality',
                component: MaterialityComponent
            },
            {
                path: 'super-users',
                component: SuperUsersListComponent
            },
            {
                path: 'super-user/materiality/:id',
                component: SuperUserMaterialityComponent
            },
            {
                path: 'super-user/disclosure/:id',
                component: SuperUserDashboardComponent
            },
            {
                path: 'profile',
                component: ProfileComponent
            }, 
            {
                path: 'organizations',
                component: OrganizationComponent
            }
        ]
    },
    { path: '**', component: LoginComponent },
   
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ],
})

export class AppRoutingModule {}