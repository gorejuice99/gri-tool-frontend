import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialityComponent } from './materiality.component';

describe('MaterialityComponent', () => {
  let component: MaterialityComponent;
  let fixture: ComponentFixture<MaterialityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
