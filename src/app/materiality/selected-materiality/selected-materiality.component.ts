import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DisclosureService } from '../../services/disclosure/disclosure.service';
@Component({
  selector: 'app-selected-materiality',
  templateUrl: './selected-materiality.component.html',
  styleUrls: ['./selected-materiality.component.scss']
})
export class SelectedMaterialityComponent implements OnInit {
  @Input('selectedItem') FormArray: any[];
  @Input('selectedUserId') selectedUserId: any;
  @Input('displaySelected') showDisplaySelected: Boolean;

  @Output() backToHomeCommand = new EventEmitter<any>();
  @Output() itemToBeRemoved = new EventEmitter<any>();
  
  showSuccessMsg: Boolean = false;
  userId: string;
  constructor(
    public route: Router,
    public disclosureSvc: DisclosureService,
    public activateRoute:ActivatedRoute
  ) { 
    this.activateRoute.params.subscribe(params=>{
      if(params.id) {
        this.userId = params.id
      } else {
        this.userId = JSON.parse(localStorage.getItem('currentUser'))._id
      }
    });
  }

  ngOnInit() {
  }

  removeItem(item): void {
    let pointer = this.FormArray.indexOf(item);
    this.itemToBeRemoved.emit(item);
    if (pointer > -1) {
      this.FormArray.splice(pointer, 1);
    }
  }

  saveItems(): void {
    let disclosureData = {
      formData: this.FormArray,
      userId: this.userId
    }
    
    this.disclosureSvc.saveDisclosureByUser(disclosureData).subscribe(snap => {
      console.log(snap);
    });
    // console.log(this.FormArray);
  }

  backToList(): void {
    this.backToHomeCommand.emit({
      "showDataTableGrid": true,
      "showDisplaySelected": true
    });
  }

  

}
