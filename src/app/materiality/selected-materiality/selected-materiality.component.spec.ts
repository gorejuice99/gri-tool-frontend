import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedMaterialityComponent } from './selected-materiality.component';

describe('SelectedMaterialityComponent', () => {
  let component: SelectedMaterialityComponent;
  let fixture: ComponentFixture<SelectedMaterialityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedMaterialityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedMaterialityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
