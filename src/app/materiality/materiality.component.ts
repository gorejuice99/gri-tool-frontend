import { Component, OnInit, Input } from '@angular/core';
import { Disclosure } from '../models/disclosure';

import { DisclosureService } from '../services/disclosure/disclosure.service';
import { User } from '../models/user';
@Component({
  selector: 'app-materiality',
  templateUrl: './materiality.component.html',
  styleUrls: ['./materiality.component.scss']
})

export class MaterialityComponent implements OnInit {
  @Input('selectedUserId') selectedUserId: any;

  showDataTableGrid: Boolean = false;
  showDisplaySelected: Boolean = true;
  
  disclosures: Array<Disclosure>;
  showBackButton: Boolean = false;
  showNextButton: Boolean = true;

  seriesNoArr: Array<any> = [200, 300, 400];
  seriesNameArr: Array<any> = ['Economic', 'Environment', 'Social'];
  seriesNo: number = 0;
  seriesName: string;

  FormArray: Array<any> = [];
  
  disclosureIdArr: Array<any> = [];
  
  curItemInfo = {
    title: '',
    guidance: '',
    reportingRequirements: [],
    additionalRequirements: [],
    recommendations: []
  }

  constructor(
    public disclosureSvc: DisclosureService
  ) { }

  ngOnInit() {
    this.seriesName = this.seriesNameArr[0];
    this.showDataTableGrid = true;
    this.callDisclosureBySeries(this.seriesNoArr[0]);
    // console.log(this.selectedUserId);
  }

  callDisclosureBySeries(seriesNo: number): void {
    this.disclosureSvc.getDisclosureBySeriesNo({
      seriesNo: seriesNo
    }).subscribe(snap => {
      snap.forEach(element => {
        let findItem = this.disclosureIdArr.indexOf(element.disclosureNumber);

        element.boolCheckbox = findItem > -1 ? true : false;
      });

      this.disclosures = snap;
    });
  }

  openSideNav(item: any, sidenav): void {

    // Set the current item's information
    // Populate the sidenav information
    this.curItemInfo.title = item.title
    this.curItemInfo.reportingRequirements = item.reportingRequirements
    this.curItemInfo.additionalRequirements = item.additionalRequirements
    this.curItemInfo.guidance = item.guidance
    this.curItemInfo.recommendations = item.recommendations

    console.log(this.curItemInfo)

    sidenav.open()

    // Prevent the click from populating
    event.cancelBubble = true
  }

  clickNext(): void {
    let counter = ++this.seriesNo;
    this.seriesName = this.seriesNameArr[counter];
    
    this.disclosures = [];
    this.callDisclosureBySeries(this.seriesNoArr[counter]);

    this.showBackButton = true;
    if(this.seriesNoArr[counter] == "400") {
      this.showNextButton = false;
    }
  }

  clickBack(): void {
    let counter = --this.seriesNo;
    this.seriesName = this.seriesNameArr[counter];

    this.disclosures = [];
    this.callDisclosureBySeries(this.seriesNoArr[counter]);

    this.showNextButton = true;
    if(this.seriesNoArr[counter] == "200") {
      this.showBackButton = false;
    }
  }

  viewSelectedItem(): void {
    this.showDataTableGrid = false;
    this.showDisplaySelected = false;
  }

  onItemClick(item): void {
    let itemSelected = item.boolCheckbox;
    console.log(item);
    console.log(itemSelected);

    if(itemSelected) {
      let index = this.FormArray.indexOf(item);
      this.FormArray.splice(index, 1);

      let index1 = this.disclosureIdArr.indexOf(item.disclosureNumber)
      this.disclosureIdArr.splice(index1, 1);
    } else {
      
      this.FormArray.push(item);
      this.disclosureIdArr.push(item.disclosureNumber);

      // console.log(this.FormArray);
      // console.log(this.disclosureIdArr);
      // console.log('here');
    }

    console.log(this.FormArray);
  }

  onItemtoBeRemoved(item: any) {
    let willRemove = this.disclosureIdArr.indexOf(item.disclosureNumber);
    if(willRemove > -1) {
      this.disclosureIdArr.splice(willRemove, 1);
    }
  }

  onHomeCommand(command: any) {
    // console.log(event);
    // console.log(command);
    this.callDisclosureBySeries(this.seriesNoArr[this.seriesNo]);

    this.showDataTableGrid = command.showDataTableGrid;
    this.showDisplaySelected = command.showDisplaySelected;
  }


}
